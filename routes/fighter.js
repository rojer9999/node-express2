const router = require('express').Router();

const { readData, handleError } = require('../repositories/fighter.repository');
const {
  findFighter,
  addFighter,
  updateFighter,
  deleteFighter
} = require('../services/fighter.service');

const validRes = (res, data) => {
  if (data) {
    res.send(data);
  } else {
    res.sendStatus(400);
    res.end();
  }
}

router.get('/', (req, res) => {
  readData()
    .then(fighters => res.send(fighters))
    .catch(handleError);
});

router.get('/:id', (req, res) => {
  findFighter(req.params.id)
    .then((fighter) => validRes(res, fighter))
    .catch(handleError);
});

router.post('/', (req, res) => {
  addFighter(req)
    .then((fighter) => validRes(res, fighter))
    .catch(handleError);
});

router.put('/:id', (req, res) => {
  updateFighter(req)
  .then((fighter) => validRes(res, fighter))
  .catch(handleError);
})

router.delete('/:id', (req, res) => {
  deleteFighter(req.params.id)
  .then((fighters) => validRes(res, fighters))
  .catch(handleError);
})

module.exports = router;
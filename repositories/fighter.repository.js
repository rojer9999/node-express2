const { createReadStream, createWriteStream } = require('fs');

const FIGHTERS = `${__dirname}/../resources/api/fighters.json`;
const getFighter = (id) => `${__dirname}/../resources/api/details/fighter/${id}.json`;

const handleError = (error) => console.log(`WARNING! ${error}`);

const readData = (id) => {
  return new Promise(resolve => {
    let data = '';
    
    createReadStream(id ? getFighter(id) : FIGHTERS, 'utf8')
      .on('data', (chunk) => data += chunk)
      .on('end', () => resolve(data))
      .on('error', (err) => console.log(err.code == 'ENOENT' ? "File wasn't found" : err));
  })
  .then((resolve) => JSON.parse(resolve))
  .catch(handleError);
}

const writeData = (data, id) => {
  return new Promise(resolve => {
    const str = JSON.stringify(data);

    const writeableStream = createWriteStream(id ? getFighter(id) : FIGHTERS);
    writeableStream.write(str);
    writeableStream.end(resolve);
    writeableStream.on('error', (err) => {
      handleError(err);
      writeableStream.end();
    })
  })
  .catch(handleError);
}

module.exports = {
  readData,
  writeData,
  handleError
}

const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');

const fighterRouter = require('./routes/fighter.js');
const indexRouter = require('./routes/index.js');

const { handleError } = require('./repositories/fighter.repository');

const PORT = process.env.PORT || 3000;
app
  .listen(PORT, () => console.log(`Server started on port ${PORT}`))
  .on('error', handleError);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());
app.options('*', cors());


app.use('/fighter', fighterRouter);
app.use('/', indexRouter);

# Readme

### Setup

1. `$ npm install`
2. Start server

### Use

- `$ npm start` - to run server
- `$ npm run dev` - to run server with nodemon

### Requests

**Get** an array of fighters:
```sh
  GET: /fighter
```

**Get** fighter with current ID:
```sh
  GET: /fighter/:id
```

**Update** fighter with data from request body with schema:
`{
  name: 'str',
  health: num,
  attack: num,
  defense: num,
  source: 'str'
}`
```sh
  PUT: /fighter/:id
```

**Delete** fighter with current ID:
```sh
DELETE: /fighter/:id
```
const validator = require('validator');

const { readData, writeData } = require('../repositories/fighter.repository');

class Fighter {
  constructor(id, name, health, attack, defense, source){
    this._id = id;
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
    this.source = source;
  }
}

const checkNewFighter = (...args) => {
  const [ name, health, attack, defense, source ] = args;

  return (
    name && validator.isLength(name, { min: 3, max: 10 }) &&
    health && health >= 1 &&
    attack && attack >= 1 &&
    defense && defense >= 1 &&
    source && source.length > 20
  );
}

const findFighter = (id) => {
  return readData(id)
    .then(data => data ? data : null);
}

const addFighter = ({
  body: { name, health, attack, defense, source },
  connection: { remoteAddress: ip }
}) => {
  return readData()
    .then(data => {
      if (checkNewFighter(name, health, attack, defense, source, ip)) {
        const id = data.length + 1;
        const newFighter = new Fighter(id, name, health, attack, defense, source);
        const newData = [...data, newFighter];
        writeData(newData);

        return newFighter;
      } else {
        return null;
      }
    });
}

const updateFighter = ({
  body: { name, health, attack, defense, source },
  params: { id }
}) => {
  return readData()
    .then(data => {
      if (checkNewFighter(name, health, attack, defense, source)) {
        const newFighter = new Fighter(id, name, health, attack, defense, source);
        writeData(newFighter, id);

        const updatedData = data.map(fighter => {
          if (fighter._id === id) {
            fighter.name = name;
            return fighter;
          }
          return fighter;
        })
        writeData(updatedData);
        return newFighter;
      } else {
        return null;
      }
    });
}

const deleteFighter = (id) => {
  return readData()
    .then(data => {
      let deletedFighter = null;
      const filteredArr = data.filter(fighter => {

        if (fighter._id !== id) {
          return true;
        } else {
          deletedFighter = fighter;
          return false;
        }
      });
      writeData(filteredArr);
      
      return deletedFighter ? deletedFighter : null;
    });
}

module.exports = {
  findFighter,
  addFighter,
  updateFighter,
  deleteFighter
}